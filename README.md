# Visium

  Vsium is mobile Android application connected to backend. Users can add their own photos with specified categories and submit them on the photo duels, where other users decide which photos are best.
That's how rankings of best photos, and best photographers are made. App is still in development phase. I'm responsible for android application, and the other team member is responsible for Web Api. 
The purpose was only to learn technologies listed below, so app will probably never be seen on the google play. 
(I will probably refactor and test some pieces of this app in the near future).
Technologies used:

  - RxJava 2
  - Dagger 2
  - Realm
  - Retrofit 2
  - Glide/Picasso
  - MVP (unfortunately not implemented everywhere)
  